import Main from './layout/main.layout';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  return (
    <div className="App">
      <Main />
    </div>
  );
}

export default App;

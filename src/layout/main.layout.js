import { useEffect, useState } from 'react'
import axios from 'axios'
import SpinnerLoader from '../components/spinner.component'
import Alert from '../components/alert.component'
import FavoriteList from '../components/favorites.component'
import Fact from '../components/fact.component'
import Search from '../components/search.component'
import { createStore } from 'redux'
import factsReducer from '../reducers/reducer'
import {Navbar, Nav, Button, Form, FormControl, NavDropdown, Container, Col, Row} from 'react-bootstrap'
import { BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';

const store = createStore(factsReducer)

function Main (){
    //Variables
    const [loaded, setDataLoaded] = useState(false) //variable que indica si se cargarón los facts o no
    const [factData, setFactData] = useState({}) //variable que guarda lo que entrega la api de chucknorris.io
    const [favorites, setFavorites] = useState([]) //lista que guarda las facts favoritas del usuario
    const [route, setRoute] = useState('https://api.chucknorris.io/jokes/random?') //variable que guarda la ruta y que será modificada para la llamada del api
    const [search, setSearch] = useState('') //variable que guarda lo que el usuario quiere buscar
    const [result, setResult] = useState([]) //variable que guarda lo se encuentra al hacer la busqueda en el search
    const [showAlert, setShowAlert] = useState([false,'']) //variable que indica si hay un error (primer elemento) y el tipo de error (segundo elemento)

    //Funcion para añadir a la lista de favoritos (store)
    const addFact = (fact) => {
        store.dispatch({ type: 'ADD', fact })
        alert("Se a guardado a tu lista de favoritos!")
    }
    // Funcion para eliminarlo de la lista de favoritos (store)
    const deleteFact = (fact) => {
        store.dispatch({ type: 'DELETE', fact })
        alert("Se a eliminado de tu lista de favoritos :c")
    }

    // Permite que al actualizarse el store se guarde lo del store en la lista de favorites, haciendo que sean practicamente iguales: favorites === store.getState()
    store.subscribe(() => {
        setFavorites(store.getState())
    })

    // Se obtiene lo de la api y como es una función asincrona permitira una vez obtenida el resultado de la api, guardarla en factData y mostrarla
    useEffect(() => {
        const fetchData = async () => {
                try {
                    if (!loaded) { 
                        const result = await axios.get(route)
                        setShowAlert([false,'']) //La alerta se hace nula y falsa
                        if(result.data.result){ //Este es para el caso de busqueda
                            setDataLoaded(true) 
                            setResult(result.data)
                            if(result.data.total === 0){ //para el caso de sin coincidencia
                                setShowAlert([true,'No se encontro coincidencia'])
                            }
                        }
                        else if (result.data) { //Este es para el caso de un random
                            setDataLoaded(true)
                            setFactData(result.data)
                        }
                    }
                } catch (error){ //Por si sucede un error inesperado
                    setDataLoaded(true)
                    setShowAlert([true,error.response.data.status + ' ' + error.response.data.error])
                }
        }
        fetchData()
    })

    // Funcion que permite cambiar la ruta según lo requerido por el usuario
    function Set_Route(c){
        if(c === ''){ //si no se busca un fact sin ninguna categoria
            setRoute('https://api.chucknorris.io/jokes/random')
        } else { //si se busca un fact con una categoria especifica
            setRoute('https://api.chucknorris.io/jokes/random?category=' + c)
        }
        GetFact()
    }
    function GetFact(){
        setResult([])
        setFactData({}) //se "elimina" el fact presente para hacer más visual que comenzará a buscar un fact nuevo y aleatorio
        setDataLoaded(false) //se hace falso el loaded para comenzar de nuevo la busqueda con la nueva ruta (o la misma)
    }

    // funcion que cambia la variable search segun lo que el usuario escriba en el buscador
    function onSearch(e){
        setSearch(e.target.value)
    }
    function SearchPressed(){
        setRoute('https://api.chucknorris.io/jokes/search?query='+ search)
        GetFact()
    }

    return (
        <Router>
                {/*    Se entrega un navbar superior en la vista principal con la que puedes acceder a las distintas vistas     */}
                <Navbar bg="dark" variant="dark">
                    <Nav className="mr-auto">
                        <Navbar.Brand>Chuck Norris</Navbar.Brand>
                    </Nav>
                </Navbar>
                <Navbar bg="dark" variant="dark" fixed = "top">
                    
                    <Link to="/"> <Navbar.Brand onClick = {()=>Set_Route('')}>Chuck Norris</Navbar.Brand> </Link>
                    <Nav className="mr-auto">
                        <NavDropdown title="Categorias" id="navbarScrollingDropdown">
                            <Container fluid><Col>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('animal')}>animal</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('career')}>career</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('celebrity')}>celebrity</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('dev')}>dev</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('explicit')}>explicit</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('fashion')}>fashion</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('food')}>food</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('history')}>history</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('money')}>money</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('movie')}>movie</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('music')}>music</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('political')}>political</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('religion')}>religion</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('science')}>science</Button></Link></Row> 
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('sport')}>sport</Button></Link></Row>
                                <Row><Link to="/"><Button variant="link" onClick = {()=>Set_Route('travel')}>travel</Button></Link></Row>
                            </Col></Container>
                        </NavDropdown>
                    </Nav>
                    <Link to="/Favoritos"><Button variant="dark">Favoritos</Button></Link>
                    <Form inline>
                        <FormControl type="text" placeholder="Buscar" onChange={onSearch} className="mr-sm-2" />
                        <Link to="/Search"><Button variant="outline-info" onClick ={SearchPressed}>Buscar</Button></Link>
                    </Form>
                </Navbar>

                {/*   Se hace uso de switches para cargar las distintas paginas necesarias para la vista   */}
                <Switch>
                    <Route path = "/Favoritos">
                        <div>
                            <FavoriteList listaFavoritos={favorites} onDelete={deleteFact}/>
                        </div>
                    </Route>
                    <Route path = "/Search">
                        <div>
                            <Search res={result} onAdd={addFact}/>
                            <Alert show={showAlert}/>
                            <SpinnerLoader dataLoaded={loaded}/>
                        </div>
                    </Route>
                    <Route path = "/">
                        <div>
                            <SpinnerLoader dataLoaded={loaded}/>
                            <Fact fact={factData} onAdd={addFact}  getFact = {GetFact}/>
                        </div>
                    </Route>
                </Switch>
            </Router>
    )

}
export default Main
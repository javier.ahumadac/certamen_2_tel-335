import { Alert } from 'react-bootstrap'

//Componente similar al spinner de la tarea, en cambio entrega una alerta

export default function AlertDismissible(props) {  //props tiene si sucede un error y el tipo de error [bool, error]
    if (!props.show[0]) {
        return null
    } else {
        return (
            <Alert variant="danger">
                <Alert.Heading>Oh noooo! Hay un error!</Alert.Heading>
                <p>
                    {props.show[1]}
                </p>
            </Alert>
            );
    }
}
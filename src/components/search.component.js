import { Button, Container, Row, Col, Card} from 'react-bootstrap'
import { useEffect, useState } from 'react'

//Se retorna una vista con los elementos de la lista de facts, recibe como parametros la lista de facts que coinciden en la busqueda,
//la funcion para agregarlo a favoritos

function Search (props) {
    
    const [resultado, setResultado] = useState([])
    useEffect(() => {
        const fetchData = async () => {
           if(props.res.result){
                setResultado(props.res.result)
            }
        }
        fetchData()
    })
    return (
        <Container>
            {
            resultado.map((fact, index) => (
                <Row className="justify-content-md-center" key={index}>
                    <Card style={{ width: '35rem' }}>
                        <Card.Body>
                            <Card.Title>{fact.value}</Card.Title>
                            <Card.Subtitle>{fact.categories}</Card.Subtitle>
                        </Card.Body>
                        <Row>
                        <Col><Button variant="success" size="sm" onClick={() => props.onAdd(fact)}>
                            Guardar
                        </Button></Col>
                        </Row>
                    </Card>
                </Row>
            ))
            }
        </Container>
    )
}

export default Search
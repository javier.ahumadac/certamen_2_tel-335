import { Button , Card, Row, Col} from 'react-bootstrap'

//Se retorna una vista con un fact, recibe como parametros variable de fact y la funcion para agregarlo a favoritos
function Fact (props) {
    
    return (
        <Row className="justify-content-md-center">
            <Card style={{ width: '35rem' }}>
                <Card.Body>
                    <Card.Title>{props.fact.value}</Card.Title>
                    <Card.Subtitle>{props.fact.categories}</Card.Subtitle>
                </Card.Body>
                <Row>
                <Col><Button variant="primary" size="sm" onClick={props.getFact}>
                    Otro
                </Button></Col>
                <Col><Button variant="success" size="sm" onClick={() => props.onAdd(props.fact)}>
                    Guardar
                </Button></Col>
                </Row>
            </Card>
        </Row>
    )
}

export default Fact
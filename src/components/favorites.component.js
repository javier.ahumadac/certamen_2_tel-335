import { Button, Container, Row, Col, Card} from 'react-bootstrap'
//Se retorna una vista con los elementos de la lista favoritos, recibe como parametros la lista de favoritos y la funcion para eliminarlos
function FavoriteList (props) {
    return (
        <Container>{
            props.listaFavoritos.map((fact, index) => (
                <Row key={index} className="justify-content-md-center">
                    <Card style={{ width: '50rem' }}>
                        <Card.Body>
                            <Card.Title>{fact.value}</Card.Title>
                        </Card.Body>
                        <Row>
                        <Col><Button variant="danger" size="sm" onClick={() => props.onDelete(fact)}>
                            Eliminar
                        </Button></Col>
                        </Row>
                    </Card>
                </Row>
            ))
            }
        </Container>
    )
}

export default FavoriteList
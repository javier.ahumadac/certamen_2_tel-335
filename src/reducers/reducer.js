function factsReducer (previousState = [], action) {
    switch (action.type) {
        // agrega a la lista total de facts favoritos la pelicula seleccionada
        case 'ADD':
            for (let index = 0; index < previousState.length; index++) {
                if(previousState[index] === action.fact){
                    return [ ...previousState]
                }               
            }
            return [ ...previousState, action.fact ]
        
        
        // entrega la lista total de facts agregadas a favoritos sin contar la pelicula seleccionada a eliminar
        case 'DELETE':
            let array = []
            for (let index = 0; index < previousState.length; index++) {
                if(previousState[index] !== action.fact){
                    array.push(previousState[index])
                }               
            }
            return array
        
        
        default:
            return previousState
    }
}

export default factsReducer